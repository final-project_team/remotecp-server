var express = require('express');
var router = express.Router();
var geoip = require("geoip-lite");
var request = require("request");
var MongoClient = require('mongodb').MongoClient;
var db_url = 'mongodb://localhost:27017/iot_server';
var ServerAddress = "http://10.8.0.6:88";

router.use(function (req, res, next) {
    if (parseInt(req.query.key) === 546337114253) {
        next();
    } else {
        res.status(403);
        res.send("Access Denied");
    }
});

function net_stat_update(body) {
    MongoClient.connect(db_url, function (err, db) {
        var collection = db.collection('devices');
        for (var dev_id in body) {
            var dev_body = body[dev_id];
            collection.updateOne(
                {"id": dev_id},
                {
                    "$push": {
                        "tx_log": {
                            $each: [{tx_bytes: dev_body['tx_bytes'], tx_packets: dev_body['tx_packets'],
                                timestamp: new Date(dev_body['timestamp'])}],
                            $slice: 87000
                        }
                    }
                },
                function (err, object) {
                    if (err) {
                        console.warn(err.message); // returns error if no matching object found
                    } else {
                        // console.dir(object); // return object with full details
                    }
                }
            );
        }
        db.close();
    });
}

router.post('/stats_update', function (req, res) {
    console.log(JSON.stringify(req.body));
    net_stat_update(req.body);
    request(ServerAddress + "/api/get_dns_log", function (error, response, body) {
        var recv_log = JSON.parse(body);
        MongoClient.connect(db_url, function (err, db) {
            var collection = db.collection('devices');
            for (var dev_id in recv_log) {
                collection.updateOne(
                    {"id": dev_id},
                    {
                        "$push": {
                            "dns_log": {
                                $each: recv_log[dev_id]
                            }
                        }
                    },
                    function (err, object) {
                        if (err) {
                            console.warn(err.message); // returns error if no matching object found
                        } else {
                            // console.dir(object); // return object with full details
                        }
                    }
                );
            }

            db.close();
        });
    });
    res.send('OK');
});

router.post('/log_connection', function (req, res) {
    console.log(JSON.stringify(req.body));
    MongoClient.connect(db_url, function (err, db) {
        var collection = db.collection('devices');
        collection.updateOne(
            {
                "id": req.body.device_id,
                "connection_log": {$elemMatch: {"ip": req.body.ip, "port": req.body.port}}
            },
            {
                $set: { "connection_log.$.timestamp": req.body.timestamp },
                $inc: { "connection_log.$.count" : 1 }
            },function (err, object) {
                if (err) {
                    console.warn(err.message); // returns error msg
                    db.close();
                } else if(object.matchedCount===0){
                    collection.updateOne(
                        {"id": req.body.device_id},
                        {
                            "$push": {
                                "connection_log": {
                                    "ip": req.body.ip,
                                    "geo": geoip.lookup(req.body.ip),
                                    "port": req.body.port,
                                    "timestamp": req.body.timestamp,
                                    "count": 1
                                }

                            }
                        },
                        function (err, object) {
                            if (err) {
                                console.warn(err.message); // returns error msg
                                db.close();
                            } else {
                                // console.dir(object); // return object with full details
                                db.close();
                            }
                        }
                    );
                } else {
                    // console.dir(object); // return object with full details
                    db.close();
                }
            }
        );

    });
    res.send('OK');
});

router.get('/whoami', function (req, res) {
    var ip = req.headers['x-real-ip'];
    ip = geoip.lookup(ip);
    res.send(JSON.stringify(ip));
});

module.exports = router;

