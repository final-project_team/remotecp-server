var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();
MongoClient = require('mongodb').MongoClient;
var db_url = 'mongodb://localhost:27017/iot_server';
var geoip = require("geoip-lite");
var port2service = require('../services');

router.get('/', function (req, res) {
    if (req.isAuthenticated()) {
        res.render('main', {
            user: req.user,
            main: "main_page"
        });
    } else {
        res.redirect('login');
    }
});

var query_limit_blacklist = {dns_log: 0, connection_log: 0, tx_bytes_log: 0, tx_packets_log: 0, tx_log: 0};
router.get('/dev/:id', function (req, res, next) {
    if (req.isAuthenticated()) {
        MongoClient.connect(db_url, function (err, db) {
            var collection = db.collection('devices');
            collection.findOne({id: req.params.id}, query_limit_blacklist, function (err, document) {
                console.log(document);
                db.close();
                if (document) {
                    res.render('manage_device', document);
                } else {
                    res.redirect('/');
                }
            });
        });
    } else {
        res.redirect('/login');
    }
});

router.get('/get_dev_panel', function (req, res) {
    if (req.isAuthenticated()) {
        res.render('partials/devicepanel', {layout: false});
    } else {
        res.redirect('login');
    }
});


//* In case we need to open registration *//

// router.get('/register', function(req, res) {
//     res.render('register');
// });
//
// router.post('/register', function(req, res) {
//     Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
//         if (err) {
//             return res.render('register', { account : account });
//         }
//
//         passport.authenticate('local')(req, res, function () {
//             res.redirect('/');
//         });
//     });
// });

function get_dns_agr_query(id) {
    return [
        {
            $match: {
                id: id
            }
        },
        {$project: {_id: 0, dns_log: 1}},
        {$unwind: "$dns_log"},
        {
            $group: {
                "_id": "$dns_log.domain",
                count: {$sum: 1},
                "timestamp": {$last: "$dns_log.ts"},
                "ip": {$last: "$dns_log.ip"}
            }
        },
        {$sort: {'count': -1}},
        {$limit: 20}
    ];
}

router.get('/get_top_dns_queries/:id', function (req, res) {
    if (req.isAuthenticated()) {
        if (req.params.id && req.params.id.length === 2) {
            MongoClient.connect(db_url, function (err, db) {
                var collection = db.collection('devices');
                collection.aggregate(get_dns_agr_query(req.params.id),
                    function (err, documents) {
                        for (var i in documents) {
                            if (documents[i].hasOwnProperty('ip')) {
                                var geo_ip = geoip.lookup(documents[i]['ip']);
                                if (geo_ip) documents[i]['geo'] = geo_ip['country'];
                            }
                        }
                        res.send(documents);
                    });
                db.close();
            });
        } else {
            res.status(404);
            res.send('not found');
        }
    } else {
        res.redirect('/login');
    }
});

// TODO maybe this is not relevant (most recent)
// router.get('/get_dns_log_sliced/:id', function (req, res) {
//     if (req.isAuthenticated()) {
//         if (req.params.id && req.params.id.length === 2) {
//             MongoClient.connect(db_url, function (err, db) {
//                 var collection = db.collection('devices');
//                 collection.findOne({id: req.params.id}, {_id: 1, dns_log: {$slice: -20}},
//                     function (err, document) {
//                         if (document.hasOwnProperty('dns_log')) {
//                             for (var i in document.dns_log) {
//                                 if (document.dns_log[i].hasOwnProperty('ip')) {
//                                     document.dns_log[i]['geo'] = geoip.lookup(document.dns_log[i]['ip'])['country'];
//                                 }
//                             }
//                             res.send(document.dns_log);
//                         } else {
//                             res.status(404);
//                             res.send('not found');
//                         }
//                     });
//                 db.close();
//             });
//         } else {
//             res.status(404);
//             res.send('not found');
//         }
//     } else {
//         res.redirect('/login');
//     }
// });

router.get('/get_dns_log/:id', function (req, res) {
    if (req.isAuthenticated()) {
        if (req.params.id && req.params.id.length === 2) {
            MongoClient.connect(db_url, function (err, db) {
                var collection = db.collection('devices');
                collection.findOne({id: req.params.id}, {_id: 1, dns_log: 1},
                    function (err, document) {
                        if (document && document.hasOwnProperty('dns_log')) {
                            for (var i in document.dns_log) {
                                if (document.dns_log[i].hasOwnProperty('ip')) {
                                    var geo_ip = geoip.lookup(document.dns_log[i]['ip']);
                                    if (geo_ip) document.dns_log[i]['geo'] = geo_ip['country'];
                                }
                            }
                            res.send(document.dns_log);
                        } else {
                            res.send(document);
                        }
                    });
                db.close();
            });
        } else {
            res.send([]);
        }
    } else {
        res.redirect('/login');
    }
});

router.get('/get_conn_log/:id', function (req, res) {
    if (req.isAuthenticated()) {
        if (req.params.id && req.params.id.length === 2) {
            MongoClient.connect(db_url, function (err, db) {
                var collection = db.collection('devices');
                collection.findOne({id: req.params.id}, {_id: 1, connection_log: 1},
                    function (err, document) {
                        if (document && document.hasOwnProperty('connection_log')) {
                            for (var i in document.connection_log) {
                                if (document.connection_log[i].hasOwnProperty('ip')) {
                                    var geo_ip = geoip.lookup(document.connection_log[i]['ip']);
                                    if (geo_ip) document.connection_log[i]['geo'] = geo_ip['country'];
                                }
                                if (document.connection_log[i].hasOwnProperty('port')) {
                                    var port = document.connection_log[i]['port'];
                                    if (port in port2service['tcp']) {
                                        document.connection_log[i]['service'] = port2service['tcp'][port];
                                    }
                                }
                            }
                            res.send(document.connection_log);
                        } else {
                            res.send(document);
                        }
                    });
                db.close();
            });
        } else {
            res.send([]);
        }
    } else {
        res.redirect('/login');
    }
});

function tx_query(id, interval, period, tx_log) {
    // time in millis - ms * secs * mins * hours * days * months
    var calc_time_period = 0;
    if (period === 'hourly') {
        calc_time_period = 1000 * 60 * 60;
    } else if (period === 'daily') {
        calc_time_period = 1000 * 60 * 60 * 24;
    } else if (period === '3_days') {
        calc_time_period = 1000 * 60 * 60 * 24 * 3;
    } else if (period === 'monthly') {
        calc_time_period = 1000 * 60 * 60 * 24 * 30;
    } else if (period === '3_months') {
        calc_time_period = 1000 * 60 * 60 * 24 * 30 * 3;
    } else {
        return;
    }
    calc_time_period += 1000 * 60 * interval;
    var prefix = "$tx_log";
    var prefix_no_dollar = "tx_log";
    var proj_obj = {'_id': 0};
    proj_obj[prefix_no_dollar] = 1;
    return [
        {$match: {'id': id}},
        {$project: proj_obj},
        {$unwind: prefix},
        {
            $group: {
                _id: {
                    "year": {"$year": prefix + ".timestamp"},
                    "dayOfYear": {"$dayOfYear": prefix + ".timestamp"},
                    "hour": {"$hour": prefix + ".timestamp"},
                    "interval": {
                        "$subtract": [
                            {"$minute": prefix + ".timestamp"},
                            {"$mod": [{"$minute": prefix + ".timestamp"}, interval]}
                        ]
                    }
                },
                "x": {"$last": "$tx_log.timestamp"},
                "y": {"$max": "$tx_log.tx_bytes"}
            }
        },
        {$match: {x: {$gt: new Date(new Date().getTime() - calc_time_period)}}},
        {"$project":{_id:0, x:1, y: 1}},
        {"$sort":{x: -1}}
    ];
}

var periods = ['hourly', 'daily', '3_days', 'monthly', '3_months'];
var period_interal = {
    'hourly': 1,
    'daily': 5,
    '3_days': 10,
    'monthly': 60,
    '3_months': 60
};

router.get('/:tx_log/:period/:id', function (req, res) {
    if (req.isAuthenticated()) {
        if ((req.params.tx_log === 'tx_bytes' || req.params.tx_log === 'tx_packets') &&
            req.params.id && req.params.id.length === 2 && periods.indexOf(req.params.period) > -1) {
            MongoClient.connect(db_url, function (err, db) {
                var collection = db.collection('devices');
                collection.aggregate(tx_query(req.params.id, period_interal[req.params.period], req.params.period, req.params.tx_log),
                    function (err, document) {
                    console.log(JSON.stringify(tx_query(req.params.id, period_interal[req.params.period], req.params.period, req.params.tx_log)));
                        res.send(document);
                    });
                db.close();
            });
        } else {
            res.send([]);
        }
    } else {
        res.redirect('/login');
    }
});

router.get('/login', function (req, res) {
    if (req.isAuthenticated()) {
        res.redirect('/');
    } else {
        res.render('login');
    }
});

router.post('/login', passport.authenticate('local', {failureRedirect: '/login'}), function (req, res) {
    res.redirect('/');
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});


module.exports = router;
