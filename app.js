var express = require('express'),
    passport = require('passport'),
    session = require('express-session'),
    path = require('path'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyparser = require('body-parser'),
    mongoose = require('mongoose'),
    routes = require('./routes/index'),
    routes_api = require('./routes/api'),
    LocalStrategy = require('passport-local').Strategy,
    Client = require('node-rest-client').Client,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    favicon = require('serve-favicon');

var app = express();
var client = new Client();
var db_url = 'mongodb://localhost:27017/iot_server';

// Block the header from containing information
// about the server
app.disable('x-powered-by');

// view engine setup
app.set('views', path.join(__dirname, '/views'));
var handlebars = require('express-handlebars').create({
    defaultLayout: 'layout',
    extname: '.hbs',
    layoutsDir: __dirname + '/views/layouts',
    partialsDir: [
        //  path to your partials
        __dirname + '/views/partials'
    ]
});
app.engine('hbs', handlebars.engine);
app.set('view engine', 'hbs');


// Required when using POST to parse encoded data
app.use(require('body-parser').urlencoded({extended: true}));

app.use(favicon(__dirname + '/public/favicon.png'));

// Defines the port to run on
server = require('http').createServer(app);
server.listen(3000, function () {
    console.log('Express started on http://localhost:' +
        server.address().port + '; press Ctrl-C to terminate');
});
var ServerAddress = "http://10.8.0.6:88"; //BOX API IP/HOSTNAME:port

var args_timeout = {
    requestConfig: {timeout: 2000},
    responseConfig: {timeout: 2000}
};
var query_limit_blacklist = {dns_log: 0, connection_log: 0, tx_bytes_log: 0, tx_packets_log: 0, tx_log: 0};
//loading socket.io
io = require('socket.io').listen(server);
io.of('/main').on('connection', function (socket) {
    console.log("Main socket.io connection");
    var update_devices = function () {
        MongoClient.connect(db_url, function (err, db) {
            assert.equal(null, err);
            var collection = db.collection('devices');
            collection.find({}, query_limit_blacklist).toArray(function (err, items) { // TODO query exclude
                console.log("UPDATE DEVICES:: ");
                console.log(items);
                socket.emit('all_net', items);
            });

            db.close();
        });
    };
    //get devices
    update_devices();

    var updates = setInterval(function () {
        // Is box alive?
        var conn = client.get(ServerAddress + "/api/alive", args_timeout, function (data, response) {
            socket.emit('conn', {'conn': data.status});
        }).on('error', function (err) {
            socket.emit('conn', {'conn': 'Not Connected'});
        });
        // Get box uptime
        var uptime = client.get(ServerAddress + "/api/uptime", args_timeout, function (data, response) {
            socket.emit('uptime', data);
        }).on('error', function (err) {
            socket.emit('uptime', {'uptime': 'N/A'});
        });
        var device_ids = client.get(ServerAddress + "/api/check_devices", args_timeout, function (data, response) {
            socket.emit('device_ids', data);
        }).on('error', function (err) {
            socket.emit('device_ids', 'N/A');
        });
        conn.on('requestTimeout', function (req) {
            socket.emit('conn', {'conn': 'Not Connected'});
            req.abort();
        });
        conn.on('responseTimeout', function (res) {
            socket.emit('conn', {'conn': 'Not Connected'});
        });
        uptime.on('requestTimeout', function (req) {
            socket.emit('uptime', {'uptime': 'N/A'});
            req.abort();
        });
        uptime.on('responseTimeout', function (res) {
            socket.emit('uptime', {'uptime': 'N/A'});
        });
        device_ids.on('requestTimeout', function (req) {
            socket.emit('device_ids', 'N/A');
            req.abort();
        });
        device_ids.on('responseTimeout', function (res) {
            socket.emit('device_ids', 'N/A');
        });
    }, 1000);


    socket.on('new_wifi_net', function () {
        client.get(ServerAddress + "/api/new_device", function (data, response) {
            data.name = "Undefined";
            data.location = "Undefined";
            data.description = "Undefined";
            MongoClient.connect(db_url, function (err, db) {
                assert.equal(null, err);
                var collection = db.collection('devices');
                collection.insertOne(data);
                db.close();
            });
            socket.emit('new_wifi_net', {
                'new_wifi_ssid': data.ssid,
                'new_wifi_key': data.key,
                'id': data.id
            });
            update_devices();
        }).on('error', function (err) {
            socket.emit('new_wifi_net', {'new_wifi_net': 'error, please try again!'});
        });
    });

    socket.on('update_new_device_to_db', function (data) {

        MongoClient.connect(db_url, function (err, db) {
            assert.equal(null, err);
            var collection = db.collection('devices');
            collection.findOneAndReplace(
                {"id": data.id},
                data
            );
            db.close();
        });
        update_devices();
    });
    socket.on('update_device', function (data) {
        console.log(data);
        MongoClient.connect(db_url, function (err, db) {
            assert.equal(null, err);
            var collection = db.collection('devices');
            collection.findOneAndUpdate(
                {"id": data.id},
                {$set: data},
                function (err, object) {
                    if (err) {
                        console.warn(err.message);  // returns error if no matching object found
                    } else {
                        console.dir(object);
                    }
                }
            );
            db.close();
        });
        update_devices();
    });

    socket.on('delete_device', function (id) {
        console.log(JSON.stringify(id));
        client.get(ServerAddress + "/api/remove_device/" + id, function (data, response) {
            MongoClient.connect(db_url, function (err, db) {
                assert.equal(null, err);
                var collection = db.collection('devices');
                console.log(id);
                collection.remove(
                    {id: id}, function (err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(id + " Deleted!");
                            update_devices();
                        }
                        db.close();
                    });

            });
        }).on('error', function (err) {
            console.log(err);
        });
    });

    socket.on('disconnect', function () {
        clearInterval(updates);
    });

});

io.of('/dev').on("connection", function (socket) {
    console.log("Device Socket.io connection");
    socket.on('subscribe', function (room) {
        console.log('joining room', room);
        socket.join(room);
        console.log(io.of('/dev').adapter.rooms);
    });
    var dev_updates = setInterval(function () {
        var rooms = io.of('/dev').adapter.rooms;
        for (var room_key in rooms){
            if (room_key.length === 2){
                var signal = client.get(ServerAddress + "/api/check_signal/id/" + room_key, args_timeout, function (data, response) {
                    io.of('/dev').to(room_key).emit('signal', data.signal);
                }).on('error', function (err) {
                    io.of('/dev').to(room_key).emit('signal', 'N/A');
                });
            }
        }
    }, 1000);
    var time_dict = {};
    var dev_updates_rt = setInterval(function () {
        var rooms = io.of('/dev').adapter.rooms;
        for (var room_key in rooms) {
            if (room_key.length === 2) {
                MongoClient.connect(db_url, function (err, db) {
                    assert.equal(null, err);
                    var collection = db.collection('devices');
                    var query;
                    if (time_dict.hasOwnProperty(room_key)) {
                        query = [
                            {"$match": {"id": room_key}},
                            {"$project": {"_id": 0, "tx_log": 1}},
                            {"$unwind": "$tx_log"},
                            {
                                "$group": {
                                    "_id": "$tx_log.timestamp",
                                    "x": {"$last": "$tx_log.timestamp"},
                                    "y": {"$max": "$tx_log.tx_bytes"}
                                }
                            },
                            {"$sort": {'_id': -1}},
                            {"$limit": 1},
                            {"$match": {"_id": {"$ne": new Date(time_dict[room_key]['_id'])}}}
                        ]
                    } else {
                        console.log(room_key);
                        query = [
                            {"$match": {"id": room_key}},
                            {"$project": {"_id": 0, "tx_log": 1}},
                            {"$unwind": "$tx_log"},
                            {
                                "$group": {
                                    "_id": "$tx_log.timestamp",
                                    "x": {"$last": "$tx_log.timestamp"},
                                    "y": {"$max": "$tx_log.tx_bytes"}
                                }
                            },
                            {"$sort": {'_id': -1}},
                            {"$limit": 1},
                            {"$match": {"_id": {"$ne": new Date()}}}
                        ]
                    }
                    collection.aggregate(query,
                        function (err, result) {
                            if (err) {
                                console.log(err);
                            } else {
                                if (result.length > 0) {
                                    time_dict[room_key] = result[0];
                                    io.of('/dev').to(room_key).emit('rt_tx', result[0]);
                                    console.log(result[0]);
                                } else {
                                }
                            }
                            db.close();
                        });
                });
            }
        }
    }, 5000);
    socket.on('disconnect', function () {
        clearInterval(dev_updates);
    });
});

app.use(logger('dev'));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(require('express-session')({
    secret: '2vg2QvYOkRhXw0l0T8o6eVtEy4lneODH',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/api', routes_api);

// passport config
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// mongoose
mongoose.connect('mongodb://localhost/passport_local_mongoose_express4');

// parseurl provides info on the url of a request object
var parseurl = require('parseurl');


// Defines a custom 404 Page and we use app.use because
// the request didn't match a route (Must follow the routes)
app.use(function (req, res) {
    // Define the content type
    res.type('text/html');

    // The default status is 200
    res.status(404);

    // Point at the 404.hbs view
    res.render('404');
});

// Custom 500 Page
app.use(function (err, req, res) {
    console.error(err.stack);
    res.status(500);

    // Point at the 500.hbs view
    res.render('500');
});

